from django import forms
from django.forms import fields


class PersonForm(forms.Form):
    name = fields.CharField(required=False)
    email = fields.CharField(required=False)