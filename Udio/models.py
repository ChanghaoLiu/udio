from django.db import models


class Person(models.Model):
    name = models.CharField(verbose_name='name', max_length=32)
    email = models.EmailField(verbose_name='email', max_length=256)