import json

from Udio.models import Person
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from Udio.serializers import PersonSerializer
from Udio.forms import PersonForm

from django.shortcuts import render


class PersonView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'person_list.html'

    def get(self, request, *args, **kwargs):
        person_form = PersonForm
        queryset = Person.objects.all()
        return Response({'profiles': queryset, 'person_form': person_form})

    def post(self,request, *args, **kwargs):
        person_form = PersonForm(request.POST)
        if person_form.is_valid():
            if person_form.cleaned_data.get('name', ''):
                name = person_form.cleaned_data.get('name')
                queryset = Person.objects.filter(name=name)
            elif person_form.cleaned_data.get('email', ''):
                email = person_form.cleaned_data.get('email')
                queryset = Person.objects.filter(email=email)
            return Response({'profiles': queryset, 'person_form': person_form})


class PersonJSView(APIView):

    def get(self, request, *args, **kwargs):
        if request.query_params.get("name"):
            queryset = Person.objects.filter(name=request.query_params.get("name"))
            ser = PersonSerializer(instance=queryset, many=True)
            return Response({"result": ser.data})
        elif request.query_params.get("email"):
            queryset = Person.objects.filter(email=request.query_params.get("email"))
            ser = PersonSerializer(instance=queryset, many=True)
            return Response({"result": ser.data})
        else:
            queryset = Person.objects.all()
            ser = PersonSerializer(instance=queryset, many=True)
            return Response({"result": ser.data})


class PersonAddView(APIView):
    def post(self, request, *args, **kwargs):
        ser = PersonSerializer(data=request.data, many=False)
        if ser.is_valid():
            ser.save()
            return Response({'profiles': "successful"})
        return Response({'profiles': "fail"})
